let debug = true;
let data = [
  { 'name': 'A', 'freq': '400', 'osc': null },
  { 'name': 'B', 'freq': '600', 'osc': null },
  { 'name': 'C4', 'freq': '800', 'osc': null },
];
let dataOverlays = [
  [
    // This is a regular scale in concert pitch
    { 'name': 'A4',  'freq': 440.00, 'color': 'red' },
    { 'name': 'A#4', 'freq': 466.16, 'color': 'red' },
    { 'name': 'B4',  'freq': 493.88, 'color': 'red' },
    { 'name': 'C5',  'freq': 523.25, 'color': 'red' },
    { 'name': 'C#5', 'freq': 554.37, 'color': 'red' },
    { 'name': 'D5',  'freq': 587.33, 'color': 'red' },
    { 'name': 'D#5', 'freq': 622.25, 'color': 'red' },
    { 'name': 'E5',  'freq': 659.26, 'color': 'red' },
    { 'name': 'F5',  'freq': 698.46, 'color': 'red' },
    { 'name': 'F#5', 'freq': 739.99, 'color': 'red' },

  ],
  [
    // Testing second overlay
    { 'name': 'a',  'freq': 460.00, 'color': 'blue' },
    { 'name': 'b', 'freq': 566.16, 'color': 'blue' },
  ]
];
let graph = null;
let midiInitialised = false;
let midiOffset = 48;
let pageShowing = 'tuner';
let synth = new Tinysynth();


window.addEventListener('load', () => {
  // LOAD
  // Init data from what's in DOM
  data = [];
  let units = document.querySelectorAll('.unit');
  for (unit of units) {
    data.push({ 'name': '', 'freq': '', 'osc': null, });
  }

  // Init MIDI
  document.body.addEventListener('click', () => {
    if (!midiInitialised) {
      navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
    }
    midiInitialised = true;
  });

  // Main menu actions
  document.querySelector('#btn-settings').addEventListener('click', showHidePage);
  document.querySelector('#btn-menu').addEventListener('click', showHidePage);
  document.querySelector('#btn-help').addEventListener('click', showHidePage);
  sliderActions();

  // Draw graph
  graph = new Graph('canvas');
  updateData();
});


function loadData(name) {
  // Load intonation from intonations.js
  console.log(intonations[name]);
  data = intonations[name];
  let root = document.querySelector('.wrapper');
  root.innerHTML = '';

  // Create sliders
  for (let i in data) {
    // Container
    let unit = _new('div');
    unit.id = i;
    unit.className = 'unit';

    // menu btn
    //let menu = _new('div');
    //menu.className = 'btn-menu-parent';
    //let img = _new('img');
    //img.className = 'btn-menu';
    //img.src = 'menu.svg';
    //unit.appendChild(menu.appendChild(img));
    unit.appendChild(_new('div'))
      .appendChild(_mk('img', {
        className: 'btn-menu',
        src: 'menu.svg',
      }));

    // name label
    let div = _new('div');
    let lbl = Object.assign(_new('input'), {
      className: 'name',
      type: 'text',
      value: data[i].name,
    });
    unit.appendChild(div.appendChild(lbl));

    // slide
    let d = _new('div');
    let s = Object.assign(_new('input'), {
      className: 'freq',
      type: 'range',
      min: 30,
      max: 800,
      step: 0.1,
    });
    s.setAttribute('orient', 'vertical');
    s.setAttribute('value', data[i].freq);
    unit.appendChild(d)
      .appendChild(s);

    // freq label
    unit.appendChild(_new('div'))
      .appendChild(_mk('input', {
        className: 'lbl-freq',
        type: 'number',
        value: data[i].freq,
        step: '0.1',
    }));

    // finetune
    let f = Object.assign(_new('input'), {
      type: 'range',
      className: 'finetune',
    });
    f.setAttribute('orient', 'vertical');
    unit.appendChild(_new('div'))
      .appendChild(f);

    // button play
    unit.appendChild(_new('div'))
      .appendChild(_mk('button', {
        className: 'btn',
        innerHTML: '>',
      }));

    // append
    root.appendChild(unit);
  }

  // TODO: Should we load these from DOM? Or directly from data?
  updateData();
  sliderActions();
};


function onMIDIFailure(msg) {
  console.log("Failed to get MIDI access - " + msg);
}


function onMIDIMessage(event) {
  // Event types:
  // - 144: note on
  // - 128: note off
  let eve = event.data[0];
  let key = event.data[1];
  let vel = event.data[2];
  if (debug) {
    console.log('EVENT', eve, key, vel);
  }
  let i = key-midiOffset;
  if (eve == 144) {
    if (key >= midiOffset) {
      data[i].osc = synth.noteOn(data[i].freq);
    }
  } else if (eve == 128) {
    if (key >= midiOffset) {
      if (data[i].osc != null) {
        data[i].osc.noteOff();
      }
    }
  }
}


function onMIDISuccess(midiAccess) {
  // MIDI has been initialised
  console.log('MIDI initialised');
  midi = midiAccess;
  uiSetMidiDevicesList();
  startLoggingMIDIInput(midi);
}


function showHidePage(e) {
  let pageClickedName = e.target.id.replace('btn-', '');
  let pageClicked = document.querySelector('#page-' + pageClickedName);
  let pageMain = document.querySelector('#page-tuner');
  let pages = document.querySelectorAll('.page');
  if (pageShowing != pageClickedName) {
    // hide all pages, show clicked page
    for (let page of pages) {
      page.style.display = 'none';
    }
    pageClicked.style.display = 'block';
    if (pageClickedName == 'menu') {
      // menu page has flex layout
      pageClicked.style.display = 'flex';
    }
    pageShowing = pageClickedName;
  } else {
    // hide clicked page, show main
    pageClicked.style.display = 'none';
    pageMain.style.display = 'block';
    pageShowing = 'tuner';
  }
  // Update title
  document.querySelector('#title').innerHTML = pageShowing;
}


function sliderActions() {
  // Slider actions
  let sliders = document.querySelectorAll('.freq');
  for (slide of sliders) {
    slide.addEventListener('input', (e) => {
      // Update label on slide move
      let i = e.target.parentElement.parentElement.id;
      //let id = e.target.id.replace('freq', '');
      //document.querySelector('#label' + id).value = e.target.value;
      document.querySelectorAll('.lbl-freq')[i].value = e.target.value;
      updateData();
    });

    slide.addEventListener('mousewheel', (e) => {
      let freq = parseFloat(parseFloat(e.target.value).toFixed(1));
      let i = e.target.parentElement.parentElement.id;
      let newValue = freq - 0.1;
      if (e.deltaY < 0) {
        newValue = freq + 0.1;
      }
      newValue = newValue.toFixed(1);
      e.target.value = newValue;
      document.querySelectorAll('.lbl-freq')[i].value = newValue;
      updateData();
      return false; // no page scrolling
    });
  } // for each slider

  // Frequency label actions
  let lbls = document.querySelectorAll('.lbl-freq');
  for (lbl of lbls) {
    lbl.addEventListener('change', (e) => {
      let i = e.target.parentElement.parentElement.id;
      document.querySelectorAll('.freq')[i].value = parseFloat(e.target.value).toFixed(1);
      e.target.value = parseFloat(e.target.value).toFixed(1);
      updateData();
    });
  }

  // Button actions
  let btns = document.querySelectorAll('.btn');
  for (btn of btns) {
    btn.addEventListener('mousedown', (e) => {
      let i = parseInt(e.target.parentElement.parentElement.id);
      if (data[i].osc == null) {
        // start osc
        data[i].osc = synth.noteOn(data[i].freq);
        e.target.innerHTML = '||';
      } else {
        // stop osc
        data[i].osc.noteOff();
        data[i].osc = null;
        e.target.innerHTML = '>';
      }
    });
  }


}


function startLoggingMIDIInput(midiAccess, indexOfPort) {
  midiAccess.inputs.forEach( function(entry) {entry.onmidimessage = onMIDIMessage;});
}


function uiSetMidiDevicesList() {
  var midi_devices = document.querySelector('#midi-devices');
  midi_devices.innerHTML = '';
  var i = 0;
  for (var entry of midi.inputs) {
    i++;
    var input = entry[1];
    var option = document.createElement('option');
    //option.value = input.id;
    option.innerHTML = input.name;
    option.value = input.id;
    if (i == midi.inputs.size) {
      option.selected = true;
    }
    midi_devices.appendChild(option);
  }
}


function updateData() {
  // Get all frequency values (from labels) and update graph
  let freqs = document.querySelectorAll('.lbl-freq');
  let names = document.querySelectorAll('.name');
  for (let i = 0; i < freqs.length; i++) {
    if (data[i].name != names[i].value) {
      // Update name
      data[i].name = names[i].value;
    }
    let freq = parseFloat(freqs[i].value);
    if (data[i].freq != freq) {
      // Update freq
      //console.log('UPDATE', i, data[i]); // DEBUG
      data[i].freq = freq;
      if (data[i].osc != null) {
        //console.log(data[i].osc); // DEBUG
        data[i].osc.setFrequency(freq);
      }
    }
  }
  graph.setData(data);
  graph.setOverlays(dataOverlays);
}

// HELPERS
function _new(name) {
  return document.createElement(name);
}


function _mk(name, props) {
  return Object.assign(_new(name), props);
}
