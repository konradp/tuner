class Graph {
  c = null;
  canvas = null;
  data = [];
  min = 420; // TODO: hardcoded
  max = 700; // TODO: hardcoded
  margin = null;
  overlays = [];
  // mouse dragging
  startDrag = null;
  dragging = false;
  initMin = this.min;
  initMax = this.max;

  constructor(canvasId) {
    const canvas = document.getElementById(canvasId);
    this.canvas = canvas;
    const c = canvas.getContext('2d');
    this.c = c;
    canvas.width = 700;
    canvas.height = 150; // TODO: Work out the pixel height/width from the canvas style size ratio
      // to make the circles round
    this.margin = canvas.width*(5/100);
    this.draw();

    // Actions
    canvas.addEventListener('wheel', (e) => {
      e.preventDefault();
      // Zoom
      if (e.deltaY < 0) {
        // zoom in
        this.min += 10;
        this.max -= 10;
      } else {
        // zoom out
        this.min -= 10;
        this.max += 10;
      }
      this.draw();
    });

    canvas.addEventListener('mousedown', (e) => {
      this.dragging = true;
      this.startDrag = e.clientX;
      this.initMin = this.min;
      this.initMax = this.max;
    });
    window.addEventListener('mouseup', () => this.dragging = false );
    canvas.addEventListener('mousemove', (e) => {
      if (this.dragging) {
        let offset = e.clientX - this.startDrag;
        let w = this.canvas.width;
        let d = this.max-this.min;
        this.min = this.initMin - d*offset/w;
        this.max = this.initMax - d*offset/w;
        this.draw();
      }
    });

  };

  setData(data) {
    // data = [
    //   { freq: 220, name: 'A3' },
    //   { freq: 400, name: 'C3' },
    //   { freq: 440, name: 'A4' },
    // ]
    this.data = data;
    this.draw();
  };

  setOverlays(data) {
    this.overlays = data;
    this.draw();

  }


  draw() {
    const c = this.c;
    const canvas = this.canvas;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);

    // Draw axis
    c.beginPath();
    c.moveTo(0, c.canvas.height-this.margin);
    c.lineTo(canvas.width, c.canvas.height-this.margin);
    c.stroke();
    c.beginPath();
    c.moveTo(0, this.margin);
    c.lineTo(canvas.width, this.margin);
    c.stroke();

    // Ruler
    //for (let i = this.min; i < this.max; i++) {
    //  c.beginPath();
    //  let pos = this.freqToPos(i);
    //  c.moveTo(pos, c.canvas.height);
    //  c.lineTo(pos, c.canvas.height-this.margin);
    //  c.stroke();
    //}

    // Draw data
    const data = this.data;
    for (let point of data) {
      this.drawBar(point.freq, point.name, point.color);
    }

    // Draw overlays
    const overlays = this.overlays;
    for (let overlayId in overlays) {
      for (let point of overlays[overlayId]) {
        this.drawBar(point.freq, point.name, point.color, overlayId);
      }
    }
  };


  drawBar(freq, name, color=null, overlayLevel=null) {
    const c = this.c;
    const pos = this.freqToPos(freq);
    const m = this.margin;
    const textHeight = 5;

    // Overlay settings
    let h = c.canvas.height - m;
    let textH = c.canvas.height - 2*this.margin;
    if (overlayLevel != null) {
      h = overlayLevel*(c.canvas.height/4);
      textH = h + 2*this.margin;
    }

    // circle
    c.beginPath();
    if (color != null) {
      c.globalAlpha = 0.5;
      c.fillStyle = color;
      c.strokeStyle = color;
    }
    const rad = 10; // TODO: rad hardcoded
    if (overlayLevel == null) {
      // user note
      c.arc(pos, h, rad, Math.PI, 0);
    } else {
      // overlays point top->down
      c.arc(pos, h+m, rad, 0, Math.PI);
    }
    c.fill();

    // Draw lines
    c.beginPath();
    if (overlayLevel == null) {
      // user notes
      c.moveTo(pos, c.canvas.height-m-rad);
      c.lineTo(pos, rad+m);
    } else {
      // overlay
      c.moveTo(pos, h+m+rad);
      c.lineTo(pos, c.canvas.height-m);
    }
    c.stroke();

    // label
    c.save();
    c.rotate(-Math.PI/2);
    c.beginPath();
    let text = c.measureText(name);
    c.textAlign = 'left';
    c.font = "1em sans";
    // TODO x hardcoded, TODO: should be text height
    if (overlayLevel == null) {
      // user note
      c.fillText(name,
        m - this.canvas.height - m + textHeight,
        pos + textHeight);
    } else {
      // overlay
      c.fillText(name,
        //-m/2 - overlayLevel*this.canvas.height/2 - text.width/2,
        - h - m,
        pos + textHeight);
    }

    // Reset colors
    c.restore();
    c.fillStyle = 'black';
    c.strokeStyle = 'black';
    c.globalAlpha = 1;
  };


  freqToPos(freq) {
    // If min=10 and max=90, map these to 0% and 100%
    //const k = (freq - this.min) / (this.max-this.min);
    //const pos = this.margin
    //  + k * (this.canvas.width - 2*this.margin);
    //const pos = (freq - this.min)/(this.max-this.min) * (this.canvas.width);
    //const pos = (freq - this.min)/(this.max-this.min) * (this.canvas.width);
    const pos = (freq-this.min)/(this.max-this.min) * (this.canvas.width);
    return pos;
  };
}; // class Graph
