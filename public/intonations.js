let intonations = {
  'demo': [
    { 'name': 'A', 'freq': 440.00, 'osc': null },
    { 'name': 'C#', 'freq': 554.37, 'osc': null },
    { 'name': 'E', 'freq': 659.26, 'osc': null },
  ],
  'equal temperament': [
    // This is a regular temperament in concert pitch
    { 'name': 'A4',  'freq': 440.00,  },
    { 'name': 'A#4', 'freq': 466.16,  },
    { 'name': 'B4',  'freq': 493.88,  },
    { 'name': 'C5',  'freq': 523.25,  },
    { 'name': 'C#5', 'freq': 554.37,  },
    { 'name': 'D5',  'freq': 587.33,  },
    { 'name': 'D#5', 'freq': 622.25,  },
    { 'name': 'E5',  'freq': 659.26,  },
    { 'name': 'F5',  'freq': 698.46,  },
    { 'name': 'F#5', 'freq': 739.99,  },
  ],
};
